README.txt.........................the file with CD contents description
secure_ota_environment..the directory with secure ota implementation
	fw_author_application.............................................
		fw_author_app................FW Author application source files
		fw_author-1.0.1-RELEASE.jar FW Author application executable

	update_server_application........................................
		fw_update_server_app.....Update Server application source files
		create.sql.....................create script for MySQL database
		fw_update_server_app-1.0.2-RELEASE.jar.......Update Server application executable

	secure_ota_interface_esp32.......................................
		example_application....example implementation of the interface
		secure_ota_esp32.h...............secure ota interface header file
		secure_ota_esp32.c ............... secure ota interface source file
		Kconfig.projbuild......................project configuration file
thesis..........................................the thesis text directory
	konicek_stepan_bachelors_thesis.pdf...the thesis in PDF format
	source...........................the directory with thesis source files
secure_OTA_update_video_demonstration.mp4....video demonstration
performance_testing_data.xlxs...table with performance testing data